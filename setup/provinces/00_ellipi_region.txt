﻿1993={ #Pîlu
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="fur"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="lullubi"
		amount=1
	}
	tribesmen={
		amount=2
	}
	tribesmen={
		culture="lullubi"
		amount=1
	}
}

5600={ #Mitutan
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="vegetables"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	freemen={
		culture="lullubi"
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		amount=1
	}
}

5604={ #Shuhhu
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="leather"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	freemen={
		culture="lullubi"
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		amount=1
	}
}

5605={ #Zizru
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="vegetables"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	freemen={
		culture="lullubi"
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		amount=1
	}
}

5653={ #Ukussu
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="base_metals"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	freemen={
		culture="lullubi"
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		amount=1
	}
}

5661={ #Digilu
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="salt"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	freemen={
		culture="lullubi"
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		amount=1
	}
}

5662={ #Ellmêshu
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="fur"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	freemen={
		culture="lullubi"
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		amount=1
	}
}

5663={ #Askuppatu
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="salt"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	freemen={
		culture="lullubi"
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		amount=1
	}
}

5664={ #Annaku
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="cattle"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	freemen={
		culture="lullubi"
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		amount=1
	}
}

6188={ #Eseru
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="precious_metals"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=3
	}
}

6194={ #Kima
	terrain="mountain_valley"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="fur"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6195={ #Batlu
	terrain="mountain_valley"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="vegetables"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6196={ #Parzillu
	terrain="mountain_valley"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="marble"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6198={ #Hadis
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="cattle"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6199={ #Dayyanum
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="marble"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6214={ #Napistum
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="cloth"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6215={ #Gishtil
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="cattle"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6216={ #Summu
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="cloth"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

6217={ #Dur-Papsukkal
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="horses"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		culture="south_akkadian"
		amount=6
	}
	freemen={
		amount=2
	}
	freemen={
		culture="south_akkadian"
		amount=4
	}
	slaves={
		amount=3
		culture="lullubi"
	}
	tribesmen={
		culture="gutian"
		amount=1
	}
}

6218={ #Immaru
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="base_metals"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	freemen={
		culture="lullubi"
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		amount=1
	}
}

6219={ #Nura
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="horses"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	freemen={
		culture="lullubi"
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		amount=1
	}
}

6220={ #Zamani
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="earthware"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	freemen={
		culture="lullubi"
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		amount=1
	}
}

6221={ #Baraqu
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="cattle"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="lullubi"
		amount=2
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		amount=1
	}
}

6222={ #Saptu
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="cattle"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		culture="lullubi"
		amount=2
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		amount=1
	}
}

6223={ #Baltu
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="cattle"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	freemen={
		culture="lullubi"
		amount=2
	}
	tribesmen={
		amount=1
	}
	tribesmen={
		culture="lullubi"
		amount=1
	}
}

6248={ #Sahatu
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="vegetables"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

6249={ #Shadu
	terrain="hills"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="earthware"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6250={ #Hursanu
	terrain="plains"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="cattle"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6251={ #Imkurgar
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="hemp"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

6252={ #Nertum
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="cloth"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6253={ #Shumsu
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="leather"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6262={ #Mamitu
	terrain="hills"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="cattle"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6263={ #Tamu
	terrain="plains"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="wild_game"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6264={ #Zakaru
	terrain="hills"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="base_metals"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6265={ #Tiamatu
	terrain="hills"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="wild_game"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6266={ #Maqlu
	terrain="hills"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="base_metals"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6267={ #Iazu
	terrain="hills"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="fur"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6268={ #Rabi
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="base_metals"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

6269={ #Istensag
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="stone"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6270={ #Petu
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="stone"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6271={ #Kirum
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="fur"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6272={ #Dudittu
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="vegetables"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6273={ #Zanu
	terrain="hills"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="vegetables"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6274={ #Nabalkutu
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="cattle"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6275={ #Qirbit
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="base_metals"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

6276={ #Ileqqe
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="cattle"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6277={ #Ekallim
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="base_metals"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6278={ #Arnum
	terrain="hills"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="stone"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6280={ #Teema
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="marble"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6281={ #Gitmalu
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="cattle"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6282={ #Iazu
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="salt"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6283={ #Sahu
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="salt"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

6284={ #Asru
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="base_metals"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6285={ #Nibruki
	terrain="mountain"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="base_metals"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

6286={ #Mul Apin
	terrain="mountain"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="fur"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

6287={ #Shammu
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="marble"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6288={ #Eresu
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="cattle"
	civilization_value=14
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6289={ #Bit-Bunakki
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="cattle"
	civilization_value=35
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=4
	}
	freemen={
		amount=5
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

6290={ #Subat
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="cattle"
	civilization_value=14
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6291={ #Salu
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="base_metals"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6292={ #Sabuh
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="base_metals"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6293={ #Emuq
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="marble"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

6294={ #Sepsu
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="salt"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

6295={ #Nisiqtu
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="vegetables"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	tribesmen={
		amount=3
	}
}

6296={ #Umisu
	terrain="desert"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="cattle"
	civilization_value=15
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

6297={ #Mahru
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="cattle"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=3
	}
}

6298={ #Tanittum
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="cattle"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

6299={ #Zagmi
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="stone"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

6322={ #Itima
	terrain="hills"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="vegetables"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6343={ #Simtim
	terrain="mountain_valley"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="precious_metals"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

6344={ #Namrasu
	terrain="hills"
	culture="ellipian"
	religion="elamite_religion"
	trade_goods="wild_game"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=3
	}
}

6434={ #Karmu
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="fur"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

6435={ #Sube'u
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="cattle"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6436={ #Salamu
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="bitumen"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6437={ #Barag
	terrain="plains"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="bitumen"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

6438={ #Eteru
	terrain="desert"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="leather"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

6439={ #Izzakkara
	terrain="desert"
	culture="gutian"
	religion="mesopotamian_religion"
	trade_goods="hemp"
	civilization_value=0
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=2
	}
}

