﻿1104={ #Astypalea
	terrain="plains"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

1147={ #Kasos
	terrain="hills"
	culture="karpathan"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=3
	}
}

1148={ #Potidaion
	terrain="farmland"
	culture="karpathan"
	religion="aegean_religion"
	trade_goods="earthware"
	civilization_value=15
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1149={ #Arkaseia
	terrain="farmland"
	culture="karpathan"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=15
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1150={ #Karpa
	terrain="hills"
	culture="karpathan"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

1152={ #Saros
	terrain="forest"
	culture="karpathan"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	tribesmen={
		amount=3
	}
}

1155={ #Ixia
	terrain="plains"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

1157={ #Arnitha
	terrain="hills"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=15
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

1158={ #Kymisala
	terrain="forest"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=15
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
}

1159={ #Kretinia
	terrain="forest"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="leather"
	civilization_value=15
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1161={ #Kameiros
	terrain="farmland"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1164={ #Damatria
	terrain="plains"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=1
	}
}

1166={ #Ialyssos
	terrain="farmland"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

1167={ #Rodos
	terrain="farmland"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="fruits"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1169={ #Brygindara
	terrain="farmland"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="fruits"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1171={ #Lindos
	terrain="farmland"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=4
	}
	tribesmen={
		amount=2
	}
}

1172={ #Ladarma
	terrain="forest"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=15
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1174={ #Laerma
	terrain="forest"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1175={ #Kiotari
	terrain="plains"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1178={ #Khalke
	terrain="forest"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1179={ #Telos
	terrain="hills"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1180={ #Nisyros
	terrain="plains"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=10
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1181={ #Onia
	terrain="forest"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1184={ #Halasarna
	terrain="hills"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1185={ #Lido
	terrain="hills"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1188={ #Kos
	terrain="farmland"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="wine"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1190={ #Kalimnos
	terrain="farmland"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="fruits"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1191={ #Vaphos
	terrain="hills"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="leather"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1199={ #Leros
	terrain="hills"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1200={ #Lepsia
	terrain="hills"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="leather"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1202={ #Patmos
	terrain="hills"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1203={ #Korsiai
	terrain="hills"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="hemp"
	civilization_value=9
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1204={ #Drakanon
	terrain="farmland"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="fruits"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

1205={ #Therma
	terrain="forest"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1207={ #Oinoe
	terrain="hills"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=20
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1208={ #Pesi
	terrain="forest"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1209={ #Kerkis
	terrain="forest"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

1211={ #Ydroussa
	terrain="farmland"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="grain"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=1
	}
}

1212={ #Pefkos
	terrain="farmland"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="cloth"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

1213={ #Pirgos
	terrain="hills"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="fruits"
	civilization_value=9
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

1214={ #Heraion
	terrain="farmland"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="grain"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=2
	}
}

1215={ #Kokkari
	terrain="hills"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1216={ #Samos
	terrain="farmland"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="fruits"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1218={ #Palaio
	terrain="hills"
	culture="leleges"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1219={ #Kepo
	terrain="farmland"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1546={ #Orna
	terrain="hills"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="fish"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

1550={ #Ahma
	terrain="farmland"
	culture="carian"
	religion="anatolian_religion"
	trade_goods="wine"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

1679={ #Ewures
	terrain="hills"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

1681={ #Anenra
	terrain="forest"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=15
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=1
	}
}

1682={ #Esin
	terrain="hills"
	culture="telchines"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=15
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

