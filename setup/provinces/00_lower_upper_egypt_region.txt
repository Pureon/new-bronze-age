﻿340={ #Basti
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="honey"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

363={ #Beqtui
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="earthware"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

370={ #Peka
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

371={ #Peshennu
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="vegetables"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

402={ #Herwer
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="earthware"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

491={ #Fet-ab
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

540={ #PROV540_egyptian
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="stone"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

542={ #PROV542_egyptian
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="cloth"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

544={ #PROV544_egyptian
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="stone"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

558={ #PROV558_egyptian
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

570={ #PROV570_egyptian
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="salt"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

594={ #PROV594_egyptian
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="leather"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

1092={ #Papo
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1119={ #Tayu-djayet
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="honey"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=8
	}
	freemen={
		amount=9
	}
	slaves={
		amount=5
	}
	tribesmen={
		amount=1
	}
}

1131={ #Nebiui
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="honey"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1151={ #Hut-nesut
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=38
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1154={ #Saka
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="papyrus"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=8
	}
	freemen={
		amount=8
	}
	slaves={
		amount=5
	}
	tribesmen={
		amount=2
	}
	holy_site = omen_anubis
}

1160={ #Per-Imen-mat-khent
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="reeds"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1163={ #Hebenu
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="fruits"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=7
	}
	freemen={
		amount=6
	}
	slaves={
		amount=4
	}
	tribesmen={
		amount=1
	}
}

1168={ #Betesh
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1176={ #Mestha
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="reeds"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1363={ #Hunt
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1364={ #Hept-shet
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="fruits"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1371={ #Hemen
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

1391={ #Sankh
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="reeds"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1399={ #Khmun
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="cloth"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=7
	}
	freemen={
		amount=9
	}
	slaves={
		amount=4
	}
	tribesmen={
		amount=1
	}
	holy_site = omen_thoth
}

1404={ #Suteniu
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1413={ #Setcha
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="papyrus"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1609={ #Mehut
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1618={ #Per-Pesht
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1619={ #Perru
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="reeds"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1636={ #Akhetaten
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="marble"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1663={ #Herrut
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1667={ #Qis
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="papyrus"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=8
	}
	freemen={
		amount=8
	}
	slaves={
		amount=5
	}
	tribesmen={
		amount=1
	}
}

1678={ #Res-her
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1836={ #Ra-heunt
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="earthware"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

1842={ #Khaibitu
	terrain="oasis"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="fruits"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=2
	}
}

2084={ #Neb Khut
	terrain="oasis"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="dates"
	civilization_value=29
	barbarian_power=3
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=3
	}
}

2282={ #Sophthis
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

2286={ #Per-Usir
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

2542={ #Per-Medjed
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="cloth"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=7
	}
	freemen={
		amount=7
	}
	slaves={
		amount=6
	}
	tribesmen={
		amount=2
	}
}

2543={ #Ageru
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="cloth"
	civilization_value=38
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

2544={ #Uabib
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

2549={ #Sesheta
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="honey"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

2571={ #Henen-nesut
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="papyrus"
	civilization_value=55
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=18
	}
	freemen={
		amount=11
	}
	slaves={
		amount=7
	}
	tribesmen={
		amount=2
	}
	holy_site = omen_heryshaf
}

2573={ #Mer-wer
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="papyrus"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=4
	}
	tribesmen={
		amount=1
	}
}

2576={ #Agrit
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=38
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

2578={ #Pehnemoun
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

2594={ #Nekha
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="vegetables"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

2596={ #Huati
	terrain="desert_hills"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="stone"
	civilization_value=19
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2602={ #Urtchu
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="cattle"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

2617={ #Neshen
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="fur"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

2619={ #Shenut-m-Qemu
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="leather"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

4629={ #Narmouthis
	terrain="oasis"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="dates"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

4653={ #Tet-ent-Ast
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="stone"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

4654={ #Ankhtet
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="fur"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

4655={ #Kaut
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="fur"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

4656={ #Neteru
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="stone"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

4657={ #Kauasha
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="cattle"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

4658={ #Thatha
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="marble"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

4659={ #Pehui
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="grain"
	civilization_value=34
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

4660={ #Sehtet
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="marble"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=6
	}
	slaves={
		amount=2
	}
	tribesmen={
		amount=1
	}
}

4661={ #Nesut
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="cattle"
	civilization_value=29
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

4662={ #Unpepet
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="earthware"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

4663={ #Henkesti
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="stone"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

4664={ #Shethu
	terrain="plains"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="stone"
	civilization_value=25
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

4665={ #Tesher
	terrain="flood_plain"
	culture="upper_egyptian"
	religion="egyptian_religion"
	trade_goods="marble"
	civilization_value=30
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

