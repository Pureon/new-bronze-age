﻿increase_civ_tech_effect = {
	if = {
		limit = {
			NOT = { has_variable = civ_tech }
		}
		set_variable = {
			name = civ_tech
			value = 0
		}
	}
	custom_tooltip = $TYPE$_header
	if = {
		limit = {
			NOR = {
				has_country_modifier = $TYPE$_2
				has_country_modifier = $TYPE$_3
				has_country_modifier = $TYPE$_4
			}
		}
		custom_tooltip = "UPGRADE_CIV_TECH_1_TT"
		custom_tooltip = $TYPE$_2_TT
		hidden_effect = {
			add_country_modifier = {
				name = $TYPE$_2
				duration = -1
			}
		}
		change_variable = {
			name = civ_tech
			add = 0.5
		}
	}
	else_if = {
		limit = {
			has_country_modifier = $TYPE$_2
		}
		custom_tooltip = "UPGRADE_CIV_TECH_2_TT"
		custom_tooltip = $TYPE$_3_TT
		hidden_effect = {
			remove_country_modifier = $TYPE$_2
			add_country_modifier = {
				name = $TYPE$_3
				duration = -1
			}
		}
		change_variable = {
			name = civ_tech
			add = 1
		}
	}
	else_if = {
		limit = {
			has_country_modifier = $TYPE$_3
		}
		custom_tooltip = "UPGRADE_CIV_TECH_3_TT"
		custom_tooltip = $TYPE$_4_TT
		hidden_effect = {
			remove_country_modifier = $TYPE$_3
			add_country_modifier = {
				name = $TYPE$_4
				duration = -1
			}
		}
		change_variable = {
			name = civ_tech
			add = 1.5
		}
	}
	hidden_effect = {
		add_country_modifier = {
			name = $TYPE$_stacking
			mode = add
			duration = -1
		}
	}
}
civ_techs_at_start_effect = {
	if = {
		limit = { #Full script
			OR = {
				primary_culture = sumerian
				country_culture_group = akkadian
				primary_culture = eblaite
				primary_culture = phoenician
				primary_culture = amoritic
				primary_culture = zalwari
				primary_culture = haburi
				primary_culture = harrani
				primary_culture = tukri
				primary_culture = lullubi
				primary_culture = gutian
				country_culture_group = egyptian
				country_culture_group = elamite
			}
			is_tribal = no
		}
		add_country_modifier = {
			name = writing_3
			duration = -1
		}
		change_variable = {
			name = civ_tech
			add = 1.5
		}
	}
	else_if = {
		limit = { #Full script
			OR = {
				primary_culture = canaanite
				primary_culture = edomite
				primary_culture = moabite
				primary_culture = ammonite
				primary_culture = golanite
				primary_culture = aramean
			}
			is_tribal = no
		}
		add_country_modifier = {
			name = writing_2
			duration = -1
		}
		change_variable = {
			name = civ_tech
			add = 0.5
		}
	}
	if = { #Urbanization
		limit = {
			capital_scope = {
				OR = {
					is_in_region = uranduruk_region
					is_in_region = isin_region
					is_in_region = kish_region
				}
			}
			can_increase_specific_tech_trigger = {
				TYPE = urbanization
			}
		}
		increase_civ_tech_effect = {
			TYPE = urbanization
		}
	}
	if = { #Irrigation
		limit = {
			capital_scope = {
				OR = {
					is_in_region = uranduruk_region
					is_in_region = isin_region
					is_in_region = kish_region
				}
			}
			can_increase_specific_tech_trigger = {
				TYPE = irrigation
			}
		}
		increase_civ_tech_effect = {
			TYPE = irrigation
		}
	}
	if = { #Bureaucracy
		limit = {
			capital_scope = {
				OR = {
					is_in_region = uranduruk_region
					is_in_region = isin_region
					is_in_region = kish_region
				}
			}
			can_increase_specific_tech_trigger = {
				TYPE = bureaucracy
			}
		}
		increase_civ_tech_effect = {
			TYPE = bureaucracy
		}
	}
	if = { #Trade
		limit = {
			primary_culture = assyrian
			can_increase_specific_tech_trigger = {
				TYPE = trade
			}
		}
		increase_civ_tech_effect = {
			TYPE = trade
		}
	}
}