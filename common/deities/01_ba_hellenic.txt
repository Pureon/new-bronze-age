﻿deity_aiolos = { #Aiolos
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_culture_trigger = {
				RELIGION = hellenic_religion
				CULTURE = aeolian
			}
			deity:omen_aiolos = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { global_migration_speed_modifier = 0.2 }
	omen = { army_movement_speed = 0.1 }
	religion = hellenic_religion
	deity_category = culture
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_assimilate_effect = yes
	}
}
deity_daidalos = { #Daidalos
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = hellenic_religion
			}
			deity:omen_daidalos = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth
	passive_modifier = { build_cost = deity_build_cost_svalue }
	omen = { research_points_modifier = omen_research_points_modifier_svalue }
	religion = hellenic_religion
	deity_category = economy
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_apotheosis_capital_citizens_effect = yes
	}
}
deity_deiwos = { #Deiwos
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = hellenic_religion
			}
			deity:omen_deiwos = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { ruler_popularity_gain = deity_ruler_popularity_gain_svalue }
	omen = { global_pop_assimilation_speed_modifier = omen_global_pop_assimilation_speed_modifier_svalue }
	religion = hellenic_religion
	deity_category = culture
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_rel_tech_effect = yes
	}
}
deity_dioskouri = { #Dioskouri
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = hellenic_religion
			}
			deity:omen_dioskouri = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_war
	passive_modifier = { monthly_experience_gain = deity_monthly_experience_gain_svalue }
	omen = { agressive_expansion_monthly_change = omen_aggressive_expansion_monthly_change_svalue }
	religion = hellenic_religion
	deity_category = war
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		military_apotheosis_military_experience_effect = yes
	}
}
deity_diwa = { #Diwa
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = hellenic_religion
			}
			deity:omen_diwa = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { monthly_tyranny = deity_monthly_tyranny_svalue }
	omen = { global_citizen_happyness = omen_global_citizen_happiness_svalue }
	religion = hellenic_religion
	deity_category = culture
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_capital_effect = yes
	}
}
deity_doros = { #Doros
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_culture_trigger = {
				RELIGION = hellenic_religion
				CULTURE = dorian
			}
			deity:omen_doros = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { monthly_political_influence_modifier = deity_monthly_political_influence_modifier_svalue }
	omen = { stability_monthly_change = omen_stability_monthly_change_svalue }
	religion = hellenic_religion
	deity_category = culture
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_assimilate_effect = yes
	}
}
deity_drimios = { #Drimios
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = hellenic_religion
			}
			deity:omen_drimios = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth
	passive_modifier = { happiness_for_same_culture_modifier = deity_happiness_for_same_culture_modifier_svalue }
	omen = { global_citizen_output = omen_global_citizen_output_svalue }
	religion = hellenic_religion
	deity_category = economy
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_apotheosis_province_improvement_effect = yes
	}
}
deity_enyalios = { #Enyalios
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = hellenic_religion
			}
			deity:omen_enyalios = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_war
	passive_modifier = { global_manpower_modifier = deity_global_manpower_modifier_svalue }
	omen = { assault_ability = omen_assault_ability_svalue }
	religion = hellenic_religion
	deity_category = war 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		military_apotheosis_manpower_effect = yes
	}
}
deity_herakles = { #Herakles
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = hellenic_religion
			}
			deity:omen_herakles = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_war
	passive_modifier = { experience_decay = deity_experience_decay_svalue }
	omen = { discipline = omen_discipline_svalue }
	religion = hellenic_religion
	deity_category = war
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		military_apotheosis_military_experience_effect = yes
	}
}
deity_ion = { #Ion
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_culture_trigger = {
				RELIGION = hellenic_religion
				CULTURE = ionian
			}
			deity:omen_ion = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { monthly_corruption = deity_monthly_corruption_svalue }
	omen = { monthly_political_influence_modifier = deity_monthly_political_influence_modifier_svalue }
	religion = hellenic_religion
	deity_category = culture
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_assimilate_effect = yes
	}
}
deity_kronos = { #Kronos
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = hellenic_religion
			}
			deity:omen_kronos = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love
	passive_modifier = { global_food_capacity = deity_global_food_capacity_svalue }
	omen = { global_monthly_food_modifier = omen_global_monthly_food_modifier }
	religion = hellenic_religion
	deity_category = fertility
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_capital_effect = yes
	}
}
deity_matere_ga = { #Matere Ga
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = hellenic_religion
			}
			deity:omen_matere_ga = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love
	passive_modifier = { global_freemen_happyness = deity_global_freemen_happyness_svalue }
	omen = { happiness_for_same_religion_modifier = omen_happiness_for_same_religion_modifier_svalue }
	religion = hellenic_religion
	deity_category = fertility
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_capital_effect = yes
	}
}
deity_paiaon = { #Paiaon
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = hellenic_religion
			}
			deity:omen_paiaon = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth
	passive_modifier = { global_capital_trade_routes = deity_global_capital_trade_routes_svalue }
	omen = { global_tax_modifier = omen_global_tax_modifier_svalue }
	religion = hellenic_religion
	deity_category = economy
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_income_effect = yes
	}
}
deity_sito_potnia = { #Sito Potnia
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = hellenic_religion
			}
			deity:omen_sito_potnia = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love
	passive_modifier = { global_population_capacity_modifier = deity_global_population_capacity_modifier_svalue }
	omen = { global_population_growth = omen_global_population_growth }
	religion = hellenic_religion
	deity_category = fertility
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_food_effect = yes
	}
}
deity_worsanos = { #Worsanos
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = hellenic_religion
			}
			deity:omen_worsanos = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { diplomatic_relations = deity_diplomatic_relations_svalue }
	omen = { ruler_popularity_gain = omen_ruler_popularity_gain_svalue }
	religion = hellenic_religion
	deity_category = culture
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_assimilate_effect = yes
	}
}