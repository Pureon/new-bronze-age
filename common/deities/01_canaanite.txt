﻿ deity_anat = { #Anat, War Goddess 
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_anat = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_war
	passive_modifier = { monthly_experience_gain = deity_monthly_experience_gain_svalue }
	omen = { discipline = omen_discipline_svalue }
	religion = canaanite_religion
	deity_category = war 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		military_apotheosis_military_experience_effect = yes
	}
}
deity_hadad = { #Hadad, maybe Aleppo/Halab, Storm God, same as Ishkur, 
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_hadad = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_war
	passive_modifier = { manpower_recovery_speed = deity_global_manpower_recovery_speed_svalue }
	omen = { land_morale_modifier = omen_land_morale_modifier_svalue }
	religion = canaanite_religion
	deity_category = war 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		military_apotheosis_manpower_effect = yes
	}
}
deity_astoreth = { #Astoreth, , Canaanite Ishtar basically
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_astoreth = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love
	passive_modifier = { global_population_capacity_modifier = deity_global_population_capacity_modifier_svalue }
	omen = { global_population_growth = omen_global_population_growth }
	religion = canaanite_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_apotheosis_capital_citizens_effect = yes
	}
}
deity_shalem = { #Shalem,Yeru-Shalem, Moon God? "The god Shalim may have been associated with dusk and the evening star in the etymological senses of a "completion" of the day, "sunset" and "peace"."
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_shalem = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { happiness_for_same_religion_modifier = deity_happiness_for_same_religion_modifier }
	omen = { ruler_popularity_gain = omen_ruler_popularity_gain_svalue }
	religion = canaanite_religion
	deity_category = culture 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_assimilate_effect = yes
	}
}
deity_bol = { #Bol, Palmyra, 
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_bol = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth
	passive_modifier = { monthly_political_influence_modifier = deity_monthly_political_influence_modifier_svalue }
	omen = { global_commerce_modifier = omen_global_commerce_modifier_svalue }
	religion = canaanite_religion
	deity_category = economy 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_apotheosis_province_improvement_effect = yes
	}
}
deity_dagon = { #Dagon, Ebla good holy site, Agriculture and fertility God
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_dagon = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love
	passive_modifier = { global_food_capacity = deity_global_food_capacity_svalue }
	omen = { global_monthly_food_modifier = omen_global_monthly_food_modifier }
	religion = canaanite_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_food_effect = yes
	}
}
deity_el = { #El, Ugarit, Father God, might be too early for him
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_el = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { global_monthly_civilization = deity_global_monthly_civilization_svalue }
	omen = { happiness_for_same_religion_modifier = omen_happiness_for_same_religion_modifier_svalue }
	religion = canaanite_religion
	deity_category = culture 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_civic_tech_effect = yes
	}
}
deity_kamish = { #Kamish/Chemosh, Moab/Ammon, War God
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_kamish = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_war
	passive_modifier = { experience_decay = -0.01 }
	omen = { manpower_recovery_speed = omen_manpower_recovery_speed }
	religion = canaanite_religion
	deity_category = war 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		military_apotheosis_military_experience_effect = yes
	}
}
deity_melqart = { #Melqart, Tyre,  ""king of the city", god of Tyre, the underworld and cycle of vegetation in Tyre, co-ruler of the underworld"
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_melqart = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth
	passive_modifier = { global_commerce_modifier = deity_global_commerce_modifier_svalue }
	omen = { global_citizen_output = omen_global_citizen_output_svalue }
	religion = canaanite_religion
	deity_category = economy 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_apotheosis_province_improvement_effect = yes
	}
}
deity_eshmun = { #Eshmun, Sidon, "god of healing and the tutelary god of Sidon" 
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_eshmun = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth
	passive_modifier = { global_capital_trade_routes = deity_global_capital_trade_routes_svalue }
	omen = { monthly_political_influence_modifier = deity_monthly_political_influence_modifier_svalue }
	religion = canaanite_religion
	deity_category = economy 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_apotheosis_capital_citizens_effect = yes
	}
}
deity_gad = { #Gad, "Gad was the name of the pan-Semitic god of fortune, usually depicted as a male but sometimes as a female"
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_gad = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth
	passive_modifier = { global_tax_modifier = deity_global_tax_modifier_svalue }
	omen = { civic_tech_investment = omen_civic_tech_investment_svalue }
	religion = canaanite_religion
	deity_category = economy 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_apotheosis_province_improvement_effect = yes
	}
}
deity_ishara = { #Ishara, One of the chief Goddesses of Ebla, Mari possible holy site as well. Išḫara is a love goddess
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_ishara = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love
	passive_modifier = { global_freemen_happyness = deity_global_freemen_happyness_svalue }
	omen = { global_pop_assimilation_speed_modifier = omen_global_pop_assimilation_speed_modifier_svalue }
	religion = canaanite_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_capital_slaves_effect = yes
	}
}
deity_kotharat = { #Kotharat, 7 goddesses of pregnancy 
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_kotharat = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love
	passive_modifier = { manpower_recovery_speed = deity_global_manpower_recovery_speed_svalue }
	omen = { global_population_growth = omen_global_population_growth }
	religion = canaanite_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_capital_effect = yes
	}
}
deity_amurru = { #Amurru, God of the Amorites, Storm God 
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_culture_trigger = {
				RELIGION = canaanite_religion
				CULTURE = amoritic
			}
			deity:omen_amurru = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { global_pop_assimilation_speed_modifier = deity_global_pop_assimilation_speed_modifier_svalue }
	omen = { global_tribesmen_happyness = omen_global_tribesmen_happiness_svalue  }
	religion = canaanite_religion
	deity_category = culture
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		
	}
}
deity_kothar_wa_khasis = { #Kothar-wa-Khasis, Ugaritic God, "Skillful-and-Wise"  "Kothar is smith, craftsman, engineer, architect, and inventor.[citation needed] He is also a soothsayer and magician, creating sacred words and incantations, in part because there is an association in many cultures of metalworking deities with magic" 
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_kothar_wa_khasis = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love
	passive_modifier = { build_cost = deity_build_cost_svalue }
	omen = { research_points_modifier = omen_research_points_modifier_svalue }
	religion = canaanite_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		
	}
}
deity_yam = { #Yam, God of the Seas, one of the Elohim, enemy of Baal Hadad
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_yam = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_war
	passive_modifier = { happiness_for_same_culture_modifier = deity_happiness_for_wrong_culture_modifier_svalue }
	omen = { ship_repair_at_sea = deity_ship_repair_at_sea }
	religion = canaanite_religion
	deity_category = war 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		naval_apotheosis_effect = yes
	}
}
deity_lotan = { #Lotan, river litani running from area of Hermon to the mediterranean (not Orontes) personification of him, Serpent, Ally of Yam, Killed by Baal Hadad, seven-headed
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_lotan = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_war
	passive_modifier = { navy_maintenance_cost = deity_navy_maintenance_cost_svalue }
	omen = { naval_damage_taken = omen_naval_damage_taken_svalue }
	religion = canaanite_religion
	deity_category = war 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		naval_apotheosis_effect = yes
	}
}
deity_marqod = { #Marqod, Phoenician God of dancing, marqod=dance 
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_culture_trigger = {
				RELIGION = canaanite_religion
				CULTURE = phoenician
			}
			deity:omen_marqod = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { happiness_for_same_culture_modifier = deity_happiness_for_same_culture_modifier_svalue }
	omen = { naval_damage_taken = omen_naval_damage_taken_svalue }
	religion = canaanite_religion
	deity_category = culture 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_civic_tech_effect = yes
	}
}
deity_molek = { #Molek, God of child sacrifice by fire, possibly symbolic, Might be too early for him
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_molek = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { global_monthly_civilization = deity_global_monthly_civilization_svalue }
	omen = { price_local_civ_button_cost_modifier = omen_global_monthly_civilization_svalue }
	religion = canaanite_religion
	deity_category = culture 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_rel_tech_effect = yes
	}
}
deity_mot = { #Mot, God of Death, Ugaritic source 
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_mot = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love
	passive_modifier = { stability_monthly_change = deity_stability_monthly_change }
	omen = { monthly_corruption = omen_monthly_corruption }
	religion = canaanite_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_capital_effect = yes
	}
}
deity_nikkal = { #Nikkal, (Nikkal-wa-ib), Goddess of Fruits
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_nikkal = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth
	passive_modifier = { global_slaves_output = deity_global_slaves_output_svalue }
	omen = { global_tax_modifier = omen_global_tax_modifier_svalue }
	religion = canaanite_religion
	deity_category = economy 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_apotheosis_capital_citizens_effect = yes
	}
}
deity_qadesh = { #Qadesh, Qadesh, "She was a fertility goddess of sacred ecstasy and sexual pleasure" 
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_qadesh = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love
	passive_modifier = { diplomatic_relations = deity_diplomatic_relations_svalue }
	omen = { diplomatic_reputation = deity_diplomatic_reputation_svalue }
	religion = canaanite_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_capital_slaves_effect = yes
	}
}
deity_resheph = { #Resheph, Eblaite(and Canaanite), "plague (or a personification of plague), war, and sometimes thunder"
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_resheph = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_war
	passive_modifier = { global_manpower_modifier = deity_global_manpower_modifier_svalue}
	omen = { agressive_expansion_monthly_change = omen_aggressive_expansion_monthly_change_svalue }
	religion = canaanite_religion
	deity_category = war 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		military_apotheosis_manpower_effect = yes
	}
}
deity_shahar = { #Shahar, Ugaritic pantheon, God of the dawn, twin of Shalim of the dusk.
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_shahar = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { monthly_wage_modifier = deity_monthly_wage_modifier_svalue }
	omen = { global_pop_conversion_speed_modifier = omen_global_pop_conversion_speed_modifier_svalue }
	religion = canaanite_religion
	deity_category = economy 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_apotheosis_province_improvement_effect = yes
	}
}
deity_shapash = { #Shapash, " Canaanite goddess of the sun,[1][2] daughter of El and Asherah. She is known as "torch of the gods"[3] and is considered an important deity in the Canaanite pantheon[4] and among the Phoenicians." 
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = canaanite_religion
				
			}
			deity:omen_shapash = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { ruler_popularity_gain = deity_ruler_popularity_gain_svalue }
	omen = { monthly_tyranny = omen_monthly_tyranny_svalue }
	religion = canaanite_religion
	deity_category = culture 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_civic_tech_effect = yes
	}
}
deity_baalat_gebal = { #Ba'alat Gebal, Byblos, literally "Lady of Byblos" (Lady as in female Lord so powerful)
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_culture_trigger = {
				RELIGION = canaanite_religion
				CULTURE = phoenician
			}
			deity:omen_baalat_gebal = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { research_points_modifier = deity_research_points_modifier_svalue }
	omen = { monthly_wage_modifier = omen_monthly_wage_modifier_svalue }
	religion = canaanite_religion
	deity_category = culture 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_civic_tech_effect = yes
	}
}