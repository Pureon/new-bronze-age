﻿# Innarawanteš
# connects the cult of Ištanuwa (city) with the river Šaḫiriya, which presumably is to be identified with the Sakarya River in north-western Anatolia.
#Lower Land include the cult of the goddess Ḫuwaššanna and some rituals
#following gods which occur in a “drinking scene” during the second day: the god(dess) Wasali of the storm god parattassi, Maliya, Wassiya, the Tarwayatt-gods, Mimiyanta, Turiyanta hurranna, the male gods, Wanama, Kursali, the great (and allinienza) gods and the Favourable Day. Then she (the queen?) drinks (to) the god Haristassi, the Gulsa-goddesses and the Favourable Day (KBo 20.72++ iii 3’–14’). During the third day of the festival, Huwassanna, the sun god, the storm god, and Suwaliyat are objects of the drinking scene (KBo 20.72++ iii 32’–37’). As most of the “minor” gods celebrated in this festival are unknown from other sources, we cannot attribute them exactly to a concrete Luwian cultic centre
deity_tarhunz = { #Tarhunz
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = anatolian_religion
			}
			deity:omen_tarhunz = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_war
	passive_modifier = { monthly_experience_gain = deity_monthly_experience_gain_svalue }
	omen = { discipline = omen_discipline_svalue }
	religion = anatolian_religion
	deity_category =  war
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		military_apotheosis_military_experience_effect = yes
	}
}
deity_tiwad = { #Tiwad, "was the Sun god. The Luwians had no female sun deity like the Hittite Sun goddess of Arinna. One of Tiwad's epithets was tati ("father"). The Late Luwian king Azatiwada ("Beloved of Tiwad") referred to him as "Tiwad of the Heavens"."
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = anatolian_religion
			}
			deity:omen_tiwad = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth
	passive_modifier = { global_tax_modifier = deity_global_tax_modifier_svalue }
	omen = { monthly_wage_modifier = omen_monthly_wage_modifier_svalue }
	religion = anatolian_religion
	deity_category = economy 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_apotheosis_province_improvement_effect = yes
	}
}
deity_kamrushipa = { #Kamrushipa, "was the wife of Tiwad and mother of the guardian god Runtiya. She played an important role in magic rituals. In Late Luwian sources, she is not attested."
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = anatolian_religion
			}
			deity:omen_kamrushipa = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { stability_monthly_change = deity_stability_monthly_change }
	omen = { monthly_corruption = omen_monthly_corruption }
	religion = anatolian_religion
	deity_category = culture
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_assimilate_effect = yes
	}
}
deity_arma = { #Arma, "was the moon god and appears in a large number of theophoric personal names (e.g. Armazati, "Man of Arma"), suggesting that he was a popular deity. In the Iron Age he completely merged with the moon god [de] of Harran and is often referred to in inscriptions as "Harranian Arma". He is depicted as a winged and bearded god with a crescent moon on his helmet. His name was written in Luwian hieroglyphs with a lunette. In curse formulae he is asked to "spear" the victim "with his horn.""
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = anatolian_religion
			}
			deity:omen_arma = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { happiness_for_wrong_culture_group_modifier = deity_happiness_for_wrong_culture_group_modifier_svalue }
	omen = { global_pop_assimilation_speed_modifier = omen_global_pop_assimilation_speed_modifier_svalue }
	religion = anatolian_religion
	deity_category = culture
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_assimilate_effect = yes
	}
}
deity_runtiya = { #Runtiya, "was a guardian god. His animal was the deer and his name was written in hieroglyphs with a deer's antlers. In Late Luwian texts, he is connected to the wilderness and serves as a god of the hunt. He is depicted as a god armed with bow and arrow, standing on a deer. His partner is the goddess Ala, who was identified with Kubaba in Kummuh"
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = anatolian_religion
			}
			deity:omen_runtiya = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love
	passive_modifier = { global_tribesmen_happyness = global_tribesmen_happiness_svalue }
	omen = { global_tribesmen_output = omen_global_tribesmen_output_svalue }
	religion = anatolian_religion
	deity_category = fertility
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_capital_effect = yes
	}
}
deity_gulza = { #Gulza, "The goddess of fate Kwanza [de] and the plague god Iyarri [de] are only attested indirectly in Late Luwian names. In the Bronze Age, the former was known as Gulza."
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = anatolian_religion
			}
			deity:omen_gulza = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_wealth
	passive_modifier = { global_citizen_happyness = deity_global_citizen_happiness_svalue }
	omen = { global_commerce_modifier = omen_global_commerce_modifier_svalue }
	religion = anatolian_religion
	deity_category = economy
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		economy_apotheosis_capital_citizens_effect = yes
	}
}
deity_lulimi = { #Lulimi, performed for the recovery of the sexual and military potency of a high-ranking officer
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = anatolian_religion
			}
			deity:omen_lulimi = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love
	passive_modifier = { global_population_capacity_modifier = deity_global_population_capacity_modifier_svalue }
	omen = { global_population_growth = omen_global_population_growth }
	religion = anatolian_religion
	deity_category = fertility
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_capital_slaves_effect = yes
	}
}
deity_kurshash = { #Kurshash, the tutelary deity of oracle birds
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = anatolian_religion
			}
			deity:omen_kurshash = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_love
	passive_modifier = { global_food_capacity = deity_global_food_capacity_svalue }
	omen = { global_monthly_food_modifier = omen_global_monthly_food_modifier }
	religion = anatolian_religion
	deity_category = fertility 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		fertility_apotheosis_food_effect = yes
	}
}
deity_yarri = { #Yarri, Plague God
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = anatolian_religion
			}
			deity:omen_yarri = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { global_monthly_civilization = deity_global_monthly_civilization_svalue }
	omen = { stability_monthly_change = omen_stability_monthly_change_svalue }
	religion = anatolian_religion
	deity_category = culture
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_assimilate_effect = yes
	}
}
deity_shanta = { #Shanta, "death-bringing god, named along with the dark Marwainzi [de], as is Nikarawa [de] in Late Luwian texts. This largely unknown deity was called upon in curses to feed an enemy to his dogs or to eat the enemy himself. Šanta was identified with the Babylonian god Marduk in the Bronze Age. His cult endured in Cilician Tarsos until classical antiquity where he was identified with Sandan-Herakles"
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = anatolian_religion
			}
			deity:omen_shanta = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_war
	passive_modifier = { global_freemen_output = deity_global_freemen_output_svalue }
	omen = { manpower_recovery_speed = omen_manpower_recovery_speed }
	religion = anatolian_religion
	deity_category = war
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		military_apotheosis_manpower_effect = yes
	}
}
deity_innarawantesh = { #Innarawanteš
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = anatolian_religion
			}
			deity:omen_innarawantesh = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_war
	passive_modifier = { land_morale_modifier = deity_land_morale_modifier_svalue }
	omen = { fabricate_claim_speed = omen_fabricate_claim_speed_svalue }
	religion = anatolian_religion
	deity_category = war
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		military_apotheosis_military_experience_effect = yes
	}
}
deity_ala = { #Ala, who was identified with Kubaba in Kummuh
	trigger = {
		polytheistic_check_religion_trigger = yes
		OR = {
			common_deity_trigger = {
				RELIGION = anatolian_religion
			}
			deity:omen_ala = {
				holy_site_deity_check_trigger = yes
			}
		}
	}
	icon = deity_eloquence
	passive_modifier = { global_capital_trade_routes = deity_global_capital_trade_routes_svalue }
	omen = { fort_maintenance_cost = omen_fort_maintenance_cost_svalue }
	religion = anatolian_religion
	deity_category = culture 
	deification_trigger = {
		can_deify_trigger = yes
	}
	
	on_activate = {
		culture_apotheosis_civic_tech_effect = yes
	}
}