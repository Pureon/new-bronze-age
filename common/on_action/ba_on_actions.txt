﻿ba_startup_pulse = {
	effect = {
		every_province = {
			if = {
				limit = {
					egyptian_drought_trigger = yes
				}
				add_permanent_province_modifier = {
					name = egyptian_drought
				}
			}
			else = {
				add_permanent_province_modifier = {
		   			name = gamestart_drought
		   		}
		   	}
		}
	}
	events = {
		ba_startup.1
		ba_startup.2
		ba_startup.5
		ba_startup.8
		ba_startup.9
	}
}
ba_egyptian_drought_lessens = {
	trigger = {
		NOT = { has_global_variable = egyptian_drought_lessened }
		current_date >= 810.1.1
	}
	effect = {
		every_province = {
			if = {
				limit = {
					egyptian_drought_trigger = yes
				}
				remove_province_modifier = egyptian_drought
				add_permanent_province_modifier = {
					name = smaller_egyptian_drought
				}
			}
			else = {
				remove_province_modifier = gamestart_drought
				add_permanent_province_modifier = {
					name = smaller_gamestart_drought
				}
			}
		}
		every_country = {
			trigger_event = {
				id = ba_startup.3
			}
			trigger_event = {
				id = ba_startup.6
			}
		}
		set_global_variable = {
			name = egyptian_drought_lessened
			value = 1
		}
	}
}
ba_egyptian_drought_removed = {
	trigger = {
		has_global_variable = egyptian_drought_lessened
		NOT = { has_global_variable = egyptian_drought_removed }
		current_date >= 840.1.1
	}
	effect = {
		every_province = {
			if = {
				limit = {
					egyptian_drought_trigger = yes
				}
				remove_province_modifier = smaller_egyptian_drought
			}
			else = {
				remove_province_modifier = smaller_gamestart_drought
			}
		}
		every_country = {
			trigger_event = {
				id = ba_startup.4
			}
			trigger_event = {
				id = ba_startup.7
			}
		}
		set_global_variable = {
			name = egyptian_drought_removed
			value = 1
		}
	}
}
egyptian_dynasty_pulse = {
	trigger = {
		government = egyptian_dynasty
	}
	effect = {
		update_dynasty_age_effect = yes
		#change prestige
	}
}

ba_colonization = {

	trigger = {
		is_ai = yes
		OR = {
			manpower >= 9
			manpower_percentage >= 0.7
		}
		current_date >= 786.1.1
		num_of_cities >= 1
		manpower_percentage >= 0.4
	}
	events = {
		expand.3
	}
}

ba_techspread_pulse = {
	trigger = {
		num_of_cities >= 1
	}
	random_events = {
		10 = ba_techspread.1
		20 = 0
	}
}
ba_claim_pulse = {
	trigger = {
		always = no
	}
	effect = {
	}
}
ba_bronze_pulse = {

	effect = {
		if = {
			limit = {
				has_country_modifier = has_bronze
			}
			if = {
				limit = {
					AND = {
						any_owned_province = {
							OR = {
								trade_goods = copper
								is_importing_trade_good = copper
							}
						}
						any_owned_province = {
							OR = {
								trade_goods = tin
								is_importing_trade_good = tin
							}
						}
					}
				}
				#nothing
			}
			else =  {
				trigger_event = {
					id = ba_tradegoods.1
				}
			}
		}
		else = {
			if = {
				limit = {
					AND = {
						any_owned_province = {
							OR = {
								trade_goods = copper
								is_importing_trade_good = copper
							}
						}
						any_owned_province = {
							OR = {
								trade_goods = tin
								is_importing_trade_good = tin
							}
						}
					}
				}
				trigger_event = {
					id = ba_tradegoods.2
				}
			}
			else =  {
				#nothing
			}
		}
	}
}
law_progression_pulse = {
	trigger = {
		is_ai = yes
	}
	effect = {
	}
}
walls_init_pulse = {
	effect = {
	}
}
tech_hack_pulse = {
	trigger = {
		is_ai = no
	}
	effect = {
		set_variable = {
			name = military_tech_hack
			value = root.military_tech
		}
		set_variable = {
			name = civic_tech_hack
			value = root.civic_tech
		}
		set_variable = {
			name = oratory_tech_hack
			value = root.oratory_tech
		}
		set_variable = {
			name = religious_tech_hack
			value = root.religious_tech
		}
	}
}
move_provincial_capital_pulse = {
	trigger = {
		is_ai = yes
	}
	effect = {
		every_country_state = {
			limit = {
				any_state_province = {
					is_state_capital = yes
					has_city_status = no
					is_capital = no
				} 
			}
			random_state_province = {
				limit = {
					has_city_status = yes
				}
				save_scope_as = targetprov
			}
			if = { limit = { exists = scope:targetprov }
				set_state_capital = scope:targetprov
			}
		}
	}
}
ba_tech_pulse = {
	trigger = {
		num_of_cities >= 1
		is_ai = yes
		has_variable = civ_tech
		trigger_if = {
			limit = { has_variable = civ_tech }
			civ_tech_tech_diff_svalue >= 0.5
		}
	}
	effect = {
		random_list = {
			1 = {
				trigger = {
					can_increase_specific_tech_trigger = {
						TYPE = palatial
					}
				}
				increase_civ_tech_effect = {
					TYPE = palatial
				}
			}
			1 = {
				trigger = {
					can_increase_specific_tech_trigger = {
						TYPE = writing
					}
				}
				increase_civ_tech_effect = {
					TYPE = writing
				}
			}
			1 = {
				trigger = {
					can_increase_specific_tech_trigger = {
						TYPE = urbanization
					}
				}
				increase_civ_tech_effect = {
					TYPE = urbanization
				}
			}
			1 = {
				trigger = {
					can_increase_specific_tech_trigger = {
						TYPE = military_structure
					}
				}
				increase_civ_tech_effect = {
					TYPE = military_structure
				}
			}
			1 = {
				trigger = {
					can_increase_specific_tech_trigger = {
						TYPE = trade
					}
				}
				increase_civ_tech_effect = {
					TYPE = trade
				}
			}
			1 = {
				trigger = {
					can_increase_specific_tech_trigger = {
						TYPE = law
					}
				}
				increase_civ_tech_effect = {
					TYPE = law
				}
			}
			1 = {
				trigger = {
					can_increase_specific_tech_trigger = {
						TYPE = bureaucracy
					}
				}
				increase_civ_tech_effect = {
					TYPE = bureaucracy
				}
			}
			1 = {
				trigger = {
					can_increase_specific_tech_trigger = {
						TYPE = irrigation
					}
				}
				increase_civ_tech_effect = {
					TYPE = irrigation
				}
			}
		}
	}
}