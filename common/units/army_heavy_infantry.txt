﻿heavy_infantry = { #Axemen
	army = yes
	assault = yes

	levy_tier = advanced

	allow = {
		trade_good_surplus = { target = copper value > 0 }
	}

	maneuver = 1
	movement_speed = 2
	build_time = 60

	light_infantry = 0.9
	camels = 1.2
	heavy_infantry = 1.0
	heavy_cavalry = 0.7
	archers = 1.0
	chariots = 0.8
	
	supply_train = 2.0
	
	attrition_weight = 0.9
	
	strength_damage_done = 1.2
	strength_damage_taken = 0.9
	morale_damage_taken = 0.9


	build_cost = {
		gold = 10
		manpower = 1
	}
	food_consumption = 0.20
	food_storage = 2.4
}