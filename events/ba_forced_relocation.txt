﻿namespace = ba_forced_relocation

ba_forced_relocation.1 = {
    type = country_event
    title = ba_forced_relocation.1.t
    desc = ba_forced_relocation.1.desc
    picture = revolt
    
    left_portrait = current_ruler

    trigger = {
    	scope:the_annexed.capital_scope = {
    		has_city_status = yes
    	}
        NOT = { root.culture = scope:actor.culture }
    }

    immediate = {
    }

    option = { #Relocate them to the core and destroy cities 
        name = ba_forced_relocation.1.a
        scope:the_annexed.capital_scope = {
        	area = {
        		every_area_province = {
        			limit = { 
        				owner = scope:actor
        				has_city_status = yes
        				valid_relocation_province_trigger = yes
        			}
                    every_pops_in_province = {
                        limit = {
                            pop_type = citizen
                        }
                        move_pop = scope:actor.capital_scope
                    }
                    destroy_city_effect = yes
		        }
		    }
        }
    }
    option = { #Don't do anything
        name = ba_forced_relocation.1.b
    }
}