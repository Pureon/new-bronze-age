﻿namespace = ba_startup

ba_startup.1 = { #Ankhtifi
    type = country_event
    title = ba_startup.1.t
    desc = ba_startup.1.desc
    picture = aqueducts
    
    left_portrait = current_ruler

    trigger = {
    	tag = BDT
    }

    immediate = {
    	# Drought
    	custom_tooltip =  "egyptian_drought_header"
    	custom_tooltip = "egyptian_drought_tt"
    }

    option = {  # Stay loyal to Henen-nesut    
        name = ba_startup.1.a
    }
    option = {  # Found own dynasty
        name = ba_startup.1.b
        break_alliance = HNN
        found_egyptian_dynasty_effect = yes
    }
}
ba_startup.2 = {
	type = country_event
    title = ba_startup.2.t
    desc = ba_startup.2.desc
    picture = aqueducts
    
    left_portrait = current_ruler

    trigger = {
    	capital_scope = {
    		is_in_egypt_trigger = yes
    	}
    	NOT = { tag = BDT }
    }

    immediate = {
   		custom_tooltip =  "egyptian_drought_header"
    	custom_tooltip = "egyptian_drought_tt"
    }

    option = {      
        name = ba_startup.2.a
    }
}
ba_startup.3 = { #Egyptian Drought Lessened Alert (effect happens in on_action)
	type = country_event
    title = ba_startup.3.t
    desc = ba_startup.3.desc
    picture = aqueducts
    
    left_portrait = current_ruler

    trigger = {
    	capital_scope = {
    		is_in_egypt_trigger = yes
    	}
    }

    immediate = {
    	custom_tooltip =  "egyptian_drought_header"
    	custom_tooltip = "egyptian_drought_smaller"
    }

    option = {      
        name = ba_startup.3.a
    }
}
ba_startup.4 = { #Egyptian Drought REMOVED Alert (effect happens in on_action)
	type = country_event
    title = ba_startup.4.t
    desc = ba_startup.4.desc
    picture = aqueducts
    
    left_portrait = current_ruler

    trigger = {
    	capital_scope = {
    		is_in_egypt_trigger = yes
    	}
    }

    immediate = {
    	custom_tooltip = "egyptian_drought_gone"
    }

    option = {      
        name = ba_startup.4.a
    }
}
ba_startup.5 = { #Non-Egyptian Drought Alert (effect happens in on_action)
	type = country_event
    title = ba_startup.5.t
    desc = ba_startup.5.desc
    picture = aqueducts
    
    left_portrait = current_ruler

    trigger = {
    	capital_scope = {
    		is_in_egypt_trigger = no
    	}
        NOR = {
            country_culture_group = gutian
            country_culture_group = elamite
            country_culture_group = lullubi
            primary_culture = tukri
            country_culture_group = sumerian
            country_culture_group = akkadian
            country_culture_group = maganite
            country_culture_group = aegean
            country_culture_group = hellenic
        }
    }

    immediate = {
    	custom_tooltip = "gamestart_drought_tt"
    }

    option = {      
        name = ba_startup.5.a
    }
}
ba_startup.6 = { #Non-Egyptian Drought LESSENED Alert (effect happens in on_action)
	type = country_event
    title = ba_startup.6.t
    desc = ba_startup.6.desc
    picture = aqueducts
    
    left_portrait = current_ruler

    trigger = {
    	capital_scope = {
    		is_in_egypt_trigger = no
    	}
    }

    immediate = {
    	custom_tooltip = "smaller_gamestart_drought_tt"
    }

    option = {      
        name = ba_startup.6.a
    }
}
ba_startup.7 = { #Non-Egyptian Drought REMOVED Alert (effect happens in on_action)
	type = country_event
    title = ba_startup.7.t
    desc = ba_startup.7.desc
    picture = aqueducts
    
    left_portrait = current_ruler

    trigger = {
    	capital_scope = {
    		is_in_egypt_trigger = no
    	}
    }

    immediate = {
    	custom_tooltip = "gamestart_drought_removed_tt"
    }

    option = {      
        name = ba_startup.7.a
    }
}
ba_startup.8 = {
    type = country_event
    title = ba_startup.8.t
    desc = ba_startup.8.desc
    picture = mesopotamian_court

    trigger = {
        capital_scope = {
            is_in_egypt_trigger = no
        }
        OR = {
            country_culture_group = gutian
            country_culture_group = elamite
            country_culture_group = lullubi
            primary_culture = tukri
            country_culture_group = sumerian
            country_culture_group = akkadian
            country_culture_group = maganite
        }
    }

    immediate = {
        custom_tooltip = "gamestart_drought_tt"
    }
    option = {      
        name = ba_startup.8.a
    }
}
ba_startup.9 = { #Startup for Proto-Achaeans and Aegeans
    type = country_event
    title = ba_startup.9.t
    desc = ba_startup.9.desc
    picture = proto_greeks

    trigger = {
        capital_scope = {
            is_in_egypt_trigger = no
        }
        OR = {
            country_culture_group = aegean
            country_culture_group = hellenic
        }
    }

    immediate = {
        custom_tooltip = "gamestart_drought_tt"
    }
    option = {      
        name = ba_startup.9.a
    }
}