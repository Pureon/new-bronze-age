﻿namespace = expand

#Colonize
expand.3 = {
	type = country_event
	hidden =  yes

	trigger = {
		num_of_cities >= 1
	}

	immediate = {
		if = {
			limit = {
				num_of_cities >= 1
			}
			random_owned_province = {
				random_neighbor_province = {
					limit = {
						has_owner = no
						is_uninhabitable = no
						is_sea = no
						total_population >= 1
						is_inhabitable = yes
						NOT = { terrain = riverine_terrain }
					}
					if = {
						limit = {
							OR = {
								terrain = desert_valley
								terrain = desert_hills
								terrain = deep_forest
								terrain = mountain
								terrain = marsh
							}
						}
						random = {
							chance = 66
							set_owned_by = root
							colonize_effect = yes
							pay_colonize_effect = yes
							save_scope_as = targetprov
							every_neighbor_province = {
								limit = {
									has_owner = yes
									NOT = {
										owner = root
									}
								}
								scope:targetprov = {
									add_claim = prev.owner
								}
							}
						}
					}
					else_if = {
						limit = { terrain = desert }
						random = {
							chance = 33
							set_owned_by = root
							colonize_effect = yes
							pay_colonize_effect = yes
							save_scope_as = targetprov
							every_neighbor_province = {
								limit = {
									has_owner = yes
									NOT = {
										owner = root
									}
								}
								scope:targetprov = {
									add_claim = prev.owner
								}
							}
						}
					}
					else = {
						set_owned_by = root
						colonize_effect = yes
						pay_colonize_effect = yes
						save_scope_as = targetprov
						every_neighbor_province = {
							limit = {
								has_owner = yes
								NOT = {
									owner = root
								}
							}
							scope:targetprov = {
								add_claim = prev.owner
							}
						}
					}
				}
			}
		}
	}
}

expand.10 = {
	type = country_event
	hidden =  yes

	trigger = {
		is_ai = yes
		num_of_cities >= 1
		capital_scope = {
			has_city_status = no
		}
		can_pay_price = price_found_city
		is_tribal = no
		war = no
	}

	immediate = {
		capital_scope = {
			found_city_effect = yes
			pay_found_city_effect = yes
		}
	}
}