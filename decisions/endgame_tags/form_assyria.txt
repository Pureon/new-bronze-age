﻿country_decisions = {
	
	form_ba_assyria = {
		
		potential = { 
			num_of_cities >= 1
			NOT = {
				tag = 1AS
			}
			NOT = {
				is_endgame_tag_trigger = yes
			}
			capital_scope = {
				is_in_region = assyria_region 
			}
			primary_culture = assyrian 
		}
		
		highlight = { 
			scope:province = { 
				OR = {
					province_id = 5303
					province_id = 5288
					province_id = 5407
					
				}
			}
		}
		
		allow = { 
			num_of_cities >= 40
			custom_tooltip = {
				text = formable_new_nation_exists
				NOT = {
					any_country = {
						tag = 1AS
					}
				}
			}
			can_form_nation_trigger = yes 
			owns_or_subject_owns = 5303
			owns_or_subject_owns = 5288
			owns_or_subject_owns = 5407
		}
		
		ai_allow = { 
			owns_or_subject_owns = 5303
			owns_or_subject_owns = 5288
			owns_or_subject_owns = 5407
		}
		
		effect = {
			change_country_name = "ASSYRIA_NAME"
			hidden_effect = {
				change_country_adjective = "ASSYRIA_ADJECTIVE"
				change_country_tag = 1AS
				change_country_color = assyria_color
			}
			capital_scope = {
				capital_formable_small_effect = yes
				formable_capital_modifier_normal_effect = yes
			}
			add_2_free_province_investments = yes
		}
		
		ai_will_do = {
			base = 1
		}
	}
} 

