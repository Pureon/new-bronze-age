﻿country_decisions = {
	
	# Form Boiotia
	conquer_egyptian_oases_decision = {
		
		potential = {
			NOT = { has_variable = enacted_conquer_oases }
			num_of_cities >= 1
			OR = {
				owns = 371
				owns = 540
				owns = 363
				owns = 340
				owns = 1946
				owns = 1939
				owns = 1895
			}
		}
		
		highlight = {
		}
		
		allow = {
		}
		
		ai_allow = {
		}
		
		effect = {
			set_variable = {
				name = enacted_conquer_oases
				value = 1
			}
			every_province = {
				limit = {
					OR = {
						is_in_area = kharga_oasis_area
						is_in_area = dakhla_oasis_area
						is_in_area = bahariya_oasis_area
						is_in_area = farafra_oasis_area
					}
				}
				add_claim = root
			}
		}
		
		ai_will_do = {
			base = 1
		}
	}
} 

