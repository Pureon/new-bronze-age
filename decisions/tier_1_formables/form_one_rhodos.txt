﻿country_decisions = {

	# Form Rhodes
	form_one_rhodos = { 
		
		potential = { 
			num_of_cities >= 1 
			NOT = {
				tag = 1RH
			}
			NOR = { 
				is_tier_1_formable_trigger = yes
				is_tier_2_formable_trigger = yes
				is_endgame_tag_trigger = yes 
			}
			capital_scope = {
				OR = {
					is_in_area = rodos_area
					is_in_area = lindos_area
				}
			}				 
			# primary_culture = 
		}
		
		highlight = { 
			scope:province = { #eponymous province territories or other important territories, one per province
				OR = {
					province_id = 1171
					province_id = 1155
					province_id = 1159
					province_id = 1166
				}
			}
		}
		
		allow = { 
			custom_tooltip = {
				text = formable_new_nation_exists
				NOT = {
					any_country = {
						tag = 1RH
					}
				}
			}
			can_form_nation_trigger = yes 
			owns_or_subject_owns = 1171
			owns_or_subject_owns = 1155
			owns_or_subject_owns = 1159
			owns_or_subject_owns = 1166
		}
		
		ai_allow = { #Doesn't have to be owned directly, vassal owning it is fine
			owns_or_subject_owns = 1171
			owns_or_subject_owns = 1155
			owns_or_subject_owns = 1159
			owns_or_subject_owns = 1166
		}
		
		effect = {
			change_country_name = "ONE_RHODOS_NAME"
			add_2_free_province_investments = yes
			capital_scope = {
				if = {
					limit = {
						root = {
							is_tribal = yes
						}
					}
					capital_formable_tribal_effect = yes
				}
				else = {
					capital_formable_small_effect = yes
				}
				formable_capital_modifier_normal_effect = yes
			}
			hidden_effect = {
				change_country_adjective = "ONE_RHODOS_ADJECTIVE"
				change_country_tag = 1RH
				change_country_color = phoenicia_color
				#change_country_flag = 
				every_province = {
					limit = {
						OR = {
							is_in_area = rodos_area
							is_in_area = lindos_area
						}
						NOT = { owner = ROOT }
					}
					add_claim = ROOT 
				}
			}
		}

		ai_will_do = {
			base = 1
		}
	}
} 