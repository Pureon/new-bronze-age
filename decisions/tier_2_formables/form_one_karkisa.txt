﻿country_decisions = {

	# Form Karkisa
	form_one_karkisa = { 
		
		potential = { 
			num_of_cities >= 1 
			NOT = {
				tag = 1KK 
			}
			NOR = { 
				is_endgame_tag_trigger = yes 
				is_tier_2_formable_trigger = yes #Tier 2 so that Arzawa or Assuwa league can be a larger formable, depending on disputed history
			}
			capital_scope = { #majority karkisan areas for simplicity
				OR = {
					is_in_region = karkisa_region 
					is_in_region = salbake_region 
					is_in_area = maiandros_area
					is_in_area = orthosia_area
					is_in_area = brioula_area
					is_in_area = apasa_area
					is_in_area = kos_area
				}
			}				 
			primary_culture = carian #only karkisan culture countries
		}
		
		highlight = { 
			scope:province = { #eponymous province territories or other important territories, one per province
				OR = {
					province_id = 1485
					province_id = 1451
					province_id = 1470
					province_id = 1510
					province_id = 1544
					province_id = 1424
					province_id = 2624
					province_id = 1610
					province_id = 1670
					province_id = 1584
					province_id = 1406
					province_id = 1651
					province_id = 1644
					province_id = 1599
				}
			}
		}
		
		allow = { 
			custom_tooltip = {
				text = formable_new_nation_exists
				NOT = {
					any_country = {
						tag = 1KR
					}
				}
			}
			can_form_nation_trigger = yes 
			owns_or_subject_owns = 1485 #Doesn't have to be owned directly, vassal owning it is fine
			owns_or_subject_owns = 1451
			owns_or_subject_owns = 1470
			owns_or_subject_owns = 1510
			owns_or_subject_owns = 1544
			owns_or_subject_owns = 1424
			owns_or_subject_owns = 2624
			owns_or_subject_owns = 1610
			owns_or_subject_owns = 1670
			owns_or_subject_owns = 1584
			owns_or_subject_owns = 1406
			owns_or_subject_owns = 1651
			owns_or_subject_owns = 1644
			owns_or_subject_owns = 1599
		}
		
		ai_allow = { 
			owns_or_subject_owns = 1485 #Doesn't have to be owned directly, vassal owning it is fine
			owns_or_subject_owns = 1451
			owns_or_subject_owns = 1470
			owns_or_subject_owns = 1510
			owns_or_subject_owns = 1544
			owns_or_subject_owns = 1424
			owns_or_subject_owns = 2624
			owns_or_subject_owns = 1610
			owns_or_subject_owns = 1670
			owns_or_subject_owns = 1584
			owns_or_subject_owns = 1406
			owns_or_subject_owns = 1651
			owns_or_subject_owns = 1644
			owns_or_subject_owns = 1599
		}

		effect = {
			change_country_name = "ONE_KARKISA_NAME"
			add_4_free_province_investments = yes
			capital_scope = {
				if = {
					limit = {
						root = {
							is_tribal = yes
						}
					}
					capital_formable_tribal_effect = yes
				}
				else = {
					capital_formable_medium_effect = yes
				}
				formable_capital_modifier_normal_effect = yes
			}
			hidden_effect = {
				change_country_adjective = "ONE_KARKISA_ADJECTIVE"
				change_country_tag = 1KK
				change_country_color = pontus_color  #Placeholder until a better idea is put forth
				#change_country_flag = Karkisa_FLAG #Haven't a clue what kind of flag this would use
				every_province = {
					limit = {
						OR = {
							is_in_region = karkisa_region 
							is_in_region = salbake_region 
							is_in_area = maiandros_area
							is_in_area = orthosia_area
							is_in_area = brioula_area
						}
						NOT = { owner = ROOT }
					}
					add_claim = ROOT 
				}
			}
		}

		ai_will_do = {
			base = 1
		}
	}
}